<?php

namespace yourock\hellowidget;

use yii\helpers\Html;

/**
 * Тестовый виджет выводящий Hello name
 * Class HelloWidget
 * @package app\components
 */
class Hellowidget extends Widget
{
	public $message;
	public $name;

	public function init()
	{
		parent::init();

		if ($this->message === null) {
			$this->message = 'Hello World';
		}

		if ($this->name === null) {
			$this->name = 'username';
		}
	}

	public function run()
	{
		// подключаем к виджету view
		return $this->render('hello', ['message' => Html::encode($this->message), 'name' => Html::encode($this->name)]);
	}
}